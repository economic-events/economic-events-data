<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" omit-xml-declaration="yes" indent="no"/>
    <xsl:template match="/">
        <!--        Host_Name,IP_address,OS,Load_avg_1min,Load_avg_5min,Load_avg_15min-->
        <xsl:for-each select="//event">
            <xsl:value-of
                    select="concat('&quot;',./eventName,'&quot;',';','&quot;',./eventCountry,'&quot;',';','&quot;',./eventCurrency,'&quot;',';','&quot;',./eventDateTime,'&quot;',';','&quot;',./eventTimeZone,'&quot;',';','&quot;',./eventPrevious,'&quot;',';','&quot;', ./eventForecast,'&quot;', ';','&quot;', ./eventActual,'&quot;', ';', '&quot;',./eventExpectedVolatility,'&quot;','&#xA;')"/>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>