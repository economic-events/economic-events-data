<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <events>
            <xsl:for-each-group select="//table/tbody/tr[starts-with(@id, 'eventRowId_')]"
                                group-by="normalize-space(./td[contains(@class, 'flagCur')]/text())">
                <xsl:element name="event-group">
                    <xsl:attribute name="currency">
                        <xsl:value-of select="normalize-space(./td[contains(@class, 'flagCur')]/text())"/>
                    </xsl:attribute>
                    <xsl:for-each select="current-group()">
                        <xsl:sort select="./@data-event-datetime"/>
                        <xsl:sort select="normalize-space(./td[contains(@class,'left event')])"/>
                        <xsl:choose>
                            <xsl:when test="./@data-event-datetime">
                                <event>
                                    <eventName>
                                        <xsl:value-of select="normalize-space(./td[contains(@class,'left event')])"/>
                                    </eventName>
                                    <eventCountry>
                                        <xsl:value-of select="./td[contains(@class, 'left flagCur')]/span/@title"/>
                                    </eventCountry>
                                    <eventCurrency>
                                        <xsl:value-of
                                                select="normalize-space(./td[contains(@class, 'flagCur')]/text())"/>
                                    </eventCurrency>
                                    <eventDateTime>
                                        <xsl:value-of
                                                select="translate(translate(@data-event-datetime,' ', 'T'), '/','-')"/>
                                    </eventDateTime>
                                    <eventTimeZone>GMT</eventTimeZone>
                                    <eventPrevious>
                                        <xsl:value-of select="./td[contains(@class,'prev')]/span/text()"/>
                                        <xsl:choose>
                                            <xsl:when test="empty(./td[contains(@class,'prev')])">
                                                empty
                                                <xsl:value-of
                                                        select="normalize-space(./td[contains(@class,'prev')]/span/text())"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of
                                                        select="normalize-space(./td[contains(@class,'prev')]/text())"/>
                                            </xsl:otherwise>
                                        </xsl:choose>

                                    </eventPrevious>
                                    <eventForecast>
                                        <xsl:value-of select="normalize-space(./td[contains(@class,'fore')]/text())"/>
                                    </eventForecast>
                                    <eventActual>
                                        <xsl:value-of select="normalize-space(./td[contains(@class, 'act')]/text())"/>
                                    </eventActual>
                                    <eventExpectedVolatility>
                                        <xsl:value-of
                                                select="substring-before(./td[contains(@class,'sentiment')]/@title, ' ')"/>
                                    </eventExpectedVolatility>
                                </event>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:for-each>
                </xsl:element>
            </xsl:for-each-group>
        </events>
    </xsl:template>

</xsl:stylesheet>