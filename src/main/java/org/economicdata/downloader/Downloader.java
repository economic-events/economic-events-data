package org.economicdata.downloader;


import com.jcabi.xml.XMLDocument;
import com.jcabi.xml.XSL;
import com.jcabi.xml.XSLDocument;
import org.economicdata.converter.XMLToCSVConverter;
import org.economicdata.converter.XMLToJSONConverter;
import org.economicdata.model.Event;
import org.economicdata.model.EventGroup;
import org.economicdata.model.Events;
import org.economicdata.model.period.Period;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Downloader {

    private static final Logger log = LoggerFactory.getLogger(Downloader.class);
    private static final XMLToJSONConverter xmlToJsonConverter = new XMLToJSONConverter();
    private static final XMLToCSVConverter xmlToCsvConverter = new XMLToCSVConverter();
    private static final int START_DATE_YEAR = 2015;
    private static final Month START_DATE_MONTH = Month.JANUARY;
    private static final int START_DATE_DAY = 1;
    private static final String ARG_LATEST = "latest";
    private static final int DATA_LATEST = 0;
    private static final int DATA_COMPLETE = 1;
    private static final Browser BROWSER = new Browser();

    public static void main(String... args) throws Exception {

        List<Period> lookupPeriods = getLookupPeriods(args.length > 0 && args[0].equals(ARG_LATEST) ? DATA_LATEST : DATA_COMPLETE);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

        WebDriver driver = BROWSER.prepareWebDriver(args[1], args[2]);

        for (Period period : lookupPeriods) {

            //Response response = null;

            log.info(new StringBuffer("Fetching raw data for the period start: ").append(period.getStartDate().format(formatter)).append(" period end: ").append(period.getEndDate().format(formatter)).toString());

            log.info("Processing raw data. Converting to reusable XML.");

            String resultingXML = transformResultingXML(prepareValidXML(BROWSER.getDataByPeriod(driver, period.getStartDate().format(formatter), period.getEndDate().format(formatter))));

            log.info("Sorting and arranging final result.");
            Map<String, Map<XMLGregorianCalendar, Events>> resultingSortedResult = sortAndArrangeResult(resultingXML);

            log.info("Persisting final results.");
            persistResult(resultingSortedResult);

            log.info(new StringBuffer("Data for the period start: ").append(period.getStartDate().format(formatter)).append(" period end: ").append(period.getEndDate().format(formatter)).append(" processed").toString());
            TimeUnit.SECONDS.sleep(3);

        }

        driver.close();
    }

    private static List<Period> getLookupPeriods(int lookupPeriod) {
        LocalDate oldDate;
        if (lookupPeriod == DATA_LATEST) {
            log.info("Looking up latest data only.");
            oldDate = LocalDate.now().minusMonths(1);
        } else {
            log.info("Looking up complete data.");
            oldDate = LocalDate.of(START_DATE_YEAR, START_DATE_MONTH, START_DATE_DAY);
        }

        LocalDate newDate = LocalDate.now();
        List<Period> periods = new ArrayList<>();
        while (oldDate.compareTo(newDate) <= 0) {
            periods.add(new Period(oldDate, oldDate.plusMonths(1).minusDays(1)));
            oldDate = oldDate.plusMonths(1);
        }
        //Adding one month ahead
        periods.add(new Period(oldDate, oldDate.plusMonths(1).minusDays(1)));

        return periods;
    }

    private static Map<String, Map<XMLGregorianCalendar, Events>> sortAndArrangeResult(String resultingXML) {
        Events events = JAXB.unmarshal(new StringReader(resultingXML), Events.class);

        Map<String, Map<XMLGregorianCalendar, Events>> resultingSortedResult = new HashMap<>();

        for (EventGroup eventGroup : events.getEventGroup()) {
            String currencyCode = eventGroup.getCurrency();
            resultingSortedResult.put(currencyCode, new HashMap<>());

            for (Event event : eventGroup.getEvent()) {

                XMLGregorianCalendar normalizedCalendar = resetCalendarTime(event.getEventDateTime());

                if (!resultingSortedResult.get(currencyCode).containsKey(normalizedCalendar)) {
                    resultingSortedResult.get(currencyCode).put(normalizedCalendar, new Events());
                }

                resultingSortedResult.get(currencyCode).get(normalizedCalendar).getEvent().add(event);
            }
        }
        return resultingSortedResult;
    }

    private static void persistResult(Map<String, Map<XMLGregorianCalendar, Events>> resultingSortedResult) throws IOException {
        for (String currency : resultingSortedResult.keySet()) {
            String currencyDataStoragePath = "data" + FileSystems.getDefault().getSeparator() + currency;

            if (Files.notExists(Paths.get(currencyDataStoragePath))) {
                Files.createDirectories(Paths.get(currencyDataStoragePath));
            }

            Map<XMLGregorianCalendar, Events> datedEvents = resultingSortedResult.get(currency);

            for (XMLGregorianCalendar calendarContainer : datedEvents.keySet()) {

                String eventsDataStoragePath = currencyDataStoragePath +
                        FileSystems.getDefault().getSeparator() +
                        calendarContainer.getYear() +
                        FileSystems.getDefault().getSeparator() +
                        calendarContainer.getMonth() +
                        FileSystems.getDefault().getSeparator() +
                        calendarContainer.getDay();

                if (Files.notExists(Paths.get(eventsDataStoragePath))) {
                    Files.createDirectories(Paths.get(eventsDataStoragePath));
                }

                Events dateEvents = datedEvents.get(calendarContainer);

                String eventsFileSoragePath = eventsDataStoragePath + FileSystems.getDefault().getSeparator() + "events.xml";


                Files.deleteIfExists(Paths.get(eventsFileSoragePath));

                log.debug("Persisting event data for " + currency + " for the " + calendarContainer.getYear() + "/" + calendarContainer.getMonth() + "/" + calendarContainer.getDay() + " in XML format");
                JAXB.marshal(new JAXBElement<Events>(new QName("", "events"), Events.class, dateEvents), Files.createFile(Paths.get(eventsFileSoragePath)).toFile());

                log.debug("Persisting event data for " + currency + " for the " + calendarContainer.getYear() + "/" + calendarContainer.getMonth() + "/" + calendarContainer.getDay() + " in JSON format");
                xmlToJsonConverter.convert(Paths.get(eventsFileSoragePath));

                log.debug("Persisting event data for " + currency + " for the " + calendarContainer.getYear() + "/" + calendarContainer.getMonth() + "/" + calendarContainer.getDay() + " in CSV format");
                xmlToCsvConverter.convert(Paths.get(eventsFileSoragePath));
            }
        }
    }

    private static XMLGregorianCalendar resetCalendarTime(XMLGregorianCalendar originalCalendar) {
        XMLGregorianCalendar normalizedCalendar = (XMLGregorianCalendar) originalCalendar.clone();
        normalizedCalendar.setHour(0);
        normalizedCalendar.setMinute(0);
        normalizedCalendar.setSecond(0);
        return normalizedCalendar;
    }

//    private static Request prepareRequest(Period period) {
//        return post("https://sslecal2.forexprostools.com/ajax.php")
//                .addHeader("Host", "sslecal2.forexprostools.com")
//                .addHeader("Connection", "keep-alive")
//                .addHeader("Accept", "application/json, text/javascript, */*; q=0.01")
//                .addHeader("X-Requested-With", "XMLHttpRequest")
//                .addHeader("User-Agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36")
//                .addHeader("Content-Type", "application/x-www-form-urlencoded")
//                .addHeader("Accept-Encoding", "gzip, deflate, br")
//                .addHeader("Accept-Language", "en-US,en;q=0.9")
//                .addFormParam("dateFrom", period.getStartDate().format(DateTimeFormatter.ISO_LOCAL_DATE))
//                .addFormParam("dateTo", period.getEndDate().format(DateTimeFormatter.ISO_LOCAL_DATE))
//                .addFormParam("timeframe", "")
//                .addFormParam("importance[]", "1")
//                .addFormParam("importance[]", "2")
//                .addFormParam("importance[]", "3")
//                .addFormParam("columns[]", "exc_flags")
//                .addFormParam("columns[]", "exc_currency")
//                .addFormParam("columns[]", "exc_importance")
//                .addFormParam("columns[]", "exc_actual")
//                .addFormParam("columns[]", "exc_forecast")
//                .addFormParam("columns[]", "exc_previous")
//                .addFormParam("timeZone", "144")
//                .addFormParam("country[]", "25")
//                .addFormParam("country[]", "32")
//                .addFormParam("country[]", "6")
//                .addFormParam("country[]", "37")
//                .addFormParam("country[]", "72")
//                .addFormParam("country[]", "22")
//                .addFormParam("country[]", "17")
//                .addFormParam("country[]", "39")
//                .addFormParam("country[]", "14")
//                .addFormParam("country[]", "10")
//                .addFormParam("country[]", "35")
//                .addFormParam("country[]", "43")
//                .addFormParam("country[]", "56")
//                .addFormParam("country[]", "36")
//                .addFormParam("country[]", "110")
//                .addFormParam("country[]", "11")
//                .addFormParam("country[]", "26")
//                .addFormParam("country[]", "12")
//                .addFormParam("country[]", "4")
//                .addFormParam("country[]", "5")
//                .addFormParam("category[]", "_employment")
//                .addFormParam("category[]", "_economicActivity")
//                .addFormParam("category[]", "_inflation")
//                .addFormParam("category[]", "_credit")
//                .addFormParam("category[]", "_centralBanks")
//                .addFormParam("category[]", "_confidenceIndex")
//                .addFormParam("category[]", "_balance")
//                .addFormParam("category[]", "_Bonds")
//                .addFormParam("action", "filter")
//                .addFormParam("lang", "1")
//                .build();
//    }

    private static String prepareValidXML(String data) {
        StringBuilder content = new StringBuilder(data.replaceAll("\\r", "")
                //.replaceAll("<thead>([\\w <>=\"/.]*)</thead>", "")
                .replaceAll("\\n", "")
                .replaceAll("\\t", "")
                .replaceAll("&nbsp;", " ")
                .replaceAll("&", "and")
                .replaceAll(";", ":"));
        //.replaceAll("evtTentative", ""));
        content.insert(0, "<table>").append("</table>");
        return content.toString();
    }

    private static String transformResultingXML(final String source) {
        InputStream xsltStream = Downloader.class.getClass().getResourceAsStream("/transformation/transformation.xslt");
        final XSL xsl = new XSLDocument(xsltStream);
        return xsl.applyTo(new XMLDocument(source));
    }


}


/*

 */
