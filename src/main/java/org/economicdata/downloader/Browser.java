package org.economicdata.downloader;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.time.Duration;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class Browser {

    public WebDriver prepareWebDriver(String username, String password) throws IOException, URISyntaxException, InterruptedException {

        WebDriver driver = getWebDriver();

        WebDriverWait wait = new WebDriverWait(driver, 300);

        //clearChromeCache(driver, wait);

        driver.get("https://www.investing.com/economic-calendar/");

        wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete"));

        ((JavascriptExecutor) driver).executeScript("window.focus();");

        login(driver, wait, username, password);

        driver.findElement(By.id("economicCurrentTime")).click();

        Sleeper.SYSTEM_SLEEPER.sleep(Duration.ofSeconds(5));

        driver.findElement(By.id("liTz55")).click();

        Sleeper.SYSTEM_SLEEPER.sleep(Duration.ofSeconds(5));

        return driver;
    }

    public String getDataByPeriod(WebDriver driver, String startDate, String endDate) throws Exception {

        WebDriverWait wait = new WebDriverWait(driver, 300);

        driver.findElement(By.cssSelector("#datePickerToggleBtn")).click();//

        Sleeper.SYSTEM_SLEEPER.sleep(Duration.ofSeconds(5));

        driver.findElement(By.cssSelector("#startDate")).clear();//#startDate
        Sleeper.SYSTEM_SLEEPER.sleep(Duration.ofSeconds(1));
        driver.findElement(By.cssSelector("#startDate")).sendKeys(startDate);

        driver.findElement(By.cssSelector("#endDate")).clear();//#endDate
        Sleeper.SYSTEM_SLEEPER.sleep(Duration.ofSeconds(1));
        driver.findElement(By.cssSelector("#endDate")).sendKeys(endDate);

        Sleeper.SYSTEM_SLEEPER.sleep(Duration.ofSeconds(3));

        driver.findElement(By.cssSelector("#applyBtn")).click();

        Sleeper.SYSTEM_SLEEPER.sleep(Duration.ofSeconds(5));


        for (int i = 0; i < 5; i++) {
            JavascriptExecutor js = ((JavascriptExecutor) driver);
            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
            Sleeper.SYSTEM_SLEEPER.sleep(Duration.ofSeconds(5));
            js.executeScript("window.scrollTo(0, 0)");
        }

        String economicCalendarRaw = driver.findElement(By.id("economicCalendarData")).getAttribute("innerHTML");

        return economicCalendarRaw;

    }

    private void login(WebDriver driver, WebDriverWait wait, String username, String password) {
        driver.findElement(By.cssSelector("#userAccount > div > a.login.bold")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#loginPopup")));

        driver.findElement(By.cssSelector("#loginFormUser_email")).sendKeys(username);

        driver.findElement(By.cssSelector("#loginForm_password")).sendKeys(password);

        driver.findElement(By.cssSelector("#signup > a")).click();

        wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete"));
    }

    private WebDriver getWebDriver() throws IOException, URISyntaxException {
        Path fullCromeDriverPath = Paths.get(Browser.class.getClass().getResource("/chromedriver/linux/x64/chromedriver").toURI());

        System.setProperty("webdriver.chrome.driver", fullCromeDriverPath.toString());

        Set<PosixFilePermission> perms = new HashSet<>();
        perms.add(PosixFilePermission.OWNER_READ);
        perms.add(PosixFilePermission.OWNER_WRITE);
        perms.add(PosixFilePermission.OWNER_EXECUTE);

        Files.setPosixFilePermissions(fullCromeDriverPath, perms);

        ChromeOptions options = new ChromeOptions();

        options.addArguments("headless");

        options.addArguments("window-size=1920,1080");

        WebDriver driver = new ChromeDriver(options);

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();

        return driver;
    }
}
