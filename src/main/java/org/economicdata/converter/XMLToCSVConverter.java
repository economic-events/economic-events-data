package org.economicdata.converter;

import com.jcabi.xml.XMLDocument;
import com.jcabi.xml.XSL;
import com.jcabi.xml.XSLDocument;
import org.economicdata.downloader.Downloader;
import org.json.JSONObject;
import org.json.XML;
import org.json.XMLParserConfiguration;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class XMLToCSVConverter {
    public void convert(Path filePath) throws IOException {
        String targetFile = filePath.toFile().getAbsolutePath().replaceAll(".xml", ".csv");

        final InputStream xsltStream = Downloader.class.getClass().getResourceAsStream("/transformation/xmlToCsv.xslt");
        final XSL xsl = new XSLDocument(xsltStream);

        String resultingCsv = xsl.applyTo(new XMLDocument(new FileInputStream(filePath.toFile())));

        Files.deleteIfExists(Paths.get(targetFile));
        Files.write(Paths.get(targetFile), resultingCsv.getBytes());


    }
}
