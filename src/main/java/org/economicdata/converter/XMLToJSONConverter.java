package org.economicdata.converter;

import org.json.JSONObject;
import org.json.XML;
import org.json.XMLParserConfiguration;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class XMLToJSONConverter {
    public void convert(Path filePath) throws IOException {
        String targetFile = filePath.toFile().getAbsolutePath().replaceAll(".xml", ".json");
        JSONObject json = XML.toJSONObject(new FileReader(filePath.toFile()), XMLParserConfiguration.KEEP_STRINGS);
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(targetFile))) {
            json.write(writer, 2, 2);
            writer.write("\n");
        }
    }
}
